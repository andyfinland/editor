; Editor functions

; Get keyboard input from editor
getKey:

	call GETCHR 
	
	; clear saved status
	call clearSaveStatus
	call goToLastSavedPos
	
	; Save
	cp CTRLW
	jp z, saveDoc
	
	; Exit
	cp CTRLE
	jp z, exitDoc
	
	; Clear
	cp CTRLK
	jp z, clearDoc
	
	; LF
	cp LF
	jp z, handleLF
	
	; CR
	cp CR
	jp z, handleCR
	
	; Tab
	cp TAB
	jp z, handleTAB
	
	; Backspace
	cp BACK
	jp z, handleBackspace
	
	; Handle ESC keys
	cp ESC
	jp z, handleESCKeys

	; Characters
	jp handleChar
	ret

; Handle ESC keys
handleESCKeys:
	
	call GETCHR

	; Navigation
	cp NAV
	jp z, handleNavKeys

	ret

; Save document
saveDoc:
	call showSavedMessage
	call goToLastSavedPos
	ld hl, IS_SAVED
	ld (hl), 1
	ret
	
; Exit document
exitDoc:

	; set NULL terminator
	ld hl, (LSTPTR)
	dec hl
	ld (hl), NULL
	dec hl
	ld (hl), NULL

	ld hl, IS_EXIT
	ld (hl), 1
	ret

; Clear document
clearDoc:
	call clearRAM
	call clearFlags
	call clearPointers
	call showBanner
	call showMenuCommands
	call showPosStatusText
	call showListPtrText
	call setStartPos
	call showTermPosStatus
	call initList
	call showListPtrAddress
	call showListPosStatus
	call moveCursorHome
	call loadDocument
	ret

; line feed
handleLF:
	
	; Check end of page
	ld a, (POSV)
	cp HEIGHT
	ret z
	
	; Add CR
	ld a, CR
	call PRINTCHR
	call addItemChr
	call incListPos
	
	; Add LF
	ld a, LF
	call PRINTCHR
	call addItemChr
	call incListPos
	
	; Add EOS
	ld a, EOS
	call addItemChr
	call incListPos

	; Add new list item
	call addListItem
	
	; update list pos
	call setStartListPos
	
	; update pos tracking
	call incVCoord
	call resetHCoord
	
	; Show terminal status
	call showListPtrAddress
	call showListPosStatus
	call showTermPosStatus
	call goToLastSavedPos
	
	ret

; characters
handleChar:

	push af

	; check if cursor is at end of the line
	ld a, (POSH) 
	cp WIDTH
	jp nz, addChar
	
	; check if cursor at end of page
	ld a, (POSV)
	cp HEIGHT
	jp nz, insertNewItem
	
	pop af
	ret
	
insertNewItem:

	; Add EOS
	ld a, EOS
	call addItemChr
	call incListPos

	; Add new list item
	call addListItem
	call setStartListPos
	
	; update pos tracking
	call incVCoord
	call resetHCoord
	call decHCoord
	
addChar:

	pop af
	
	; Add char to list item
	call PRINTCHR
	call addItemChr
	
	; update pos tracking
	call incHCoord
	call incListPos
	
	; Show terminal status
	call showListPtrAddress
	call showListPosStatus
	call showTermPosStatus
	call goToLastSavedPos
	
	; Add default EOS
	ld a, EOS
	call addItemChr
	
	ret


; Handle CR
handleCR:
	ret
	
; Handle TAB
handleTAB:

	ld a, ' '
	call handleChar
	ld a, ' '
	call handleChar
	ld a, ' '
	call handleChar
	ret
	
; handle backspace
handleBackspace:

	; Handle wrapping
	ld a, (POSH)
	dec a 
	cp 0
	ret z
	
	; move cursor
	ld hl, moveLeft
	call PRINTSTR
	ld a, ' '
	call PRINTCHR
	ld hl, moveLeft
	call PRINTSTR
	
	; uodate pos tracking
	call decHCoord
	call decListPos
	
	; Show terminal status
	call showListPtrAddress
	call showListPosStatus
	call showTermPosStatus
	call goToLastSavedPos
	
	ret


