; Archive

; Print contents of buffer
; Destroys A
printBuffer:

	; check if buffer is empty
	ld hl, BUFPOS
	ld a, (hl)
	cp 0
	ret z
	
	; display contents
	ld hl, DOCBUF
	call PRINTSTR
	ret
	
; Add to document buffer
; Input A = char
addBuffer:

	push af
	ld hl, (BUFPOS)
	ld de, DOCBUF
	add hl, de
	ld (hl), a
	inc hl
	ld (hl), EOS
	pop af
	ret

; Remove char from document buffer
; Destroys A
removeBuffer:
	ld hl, (BUFPOS)		; current pos
	ld de, DOCBUF		; staring point of cuffer
	add hl, de			; hl = current pos
	
loopBuffer:

	; check if address contains EOS
	ld a, (hl)
	cp EOS
	ret z
	
	; copy
	inc hl
	inc hl
	ld a, (hl)
	dec hl
	ld (hl), a
	
	; goto next address
	inc hl
	
	jr loopBuffer
	ret

; inc buffer pos
; Destroys DE
incBufferPos:
	push af
	ld hl, (BUFPOS)
	ld de, 1
	add hl, de
	ld (BUFPOS), hl
	pop af
	ret
	
; dec buffer pos
decBufferPos:
	push af
	ld hl, (BUFPOS)
	ld de, 1
	add a
	sbc hl, de
	ld (BUFPOS), hl
	pop af
	ret

; Update buffer position status
; Destroys A, DE, HL
updateBufferPosStatus:
	push af
	ld d, 40
	ld e, 67
	call moveCursor
	ld hl, (BUFPOS)
	call print16bitDec2asciiXXXX
	call goToLastPos
	pop af
	ret
	
	
; characters
handleChr:
	
	push af
	
	; check end of line
	ld a, (POSH)
	dec a ; >80
	cp WIDTH 
	call z, handleChrWrap
	
	pop af
	
	; otherwise add chars to buffer
	call incBufferPos
	call addBuffer
	call PRINTCHR
	call incHCoord
	call updateBufferPosStatus
	ret
	
handleChrWrap:
	call incVCoord
	call resetH
	ret