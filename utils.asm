; Utils

; Clear RAM
; Destroys HL,DE,BC
clearRAM:
    ld hl,DOCBUF
    ld e,l
    ld d,h
    inc de
    ld (hl),0
    ld bc,DOCSIZ
    ldir
	ret


; Prints 16bit number in HL as decimal ASCII
; Input: HL
; Destroys BC, A, HL
; Output ASCII (padded e.g. 00200)
print16bitDec2asciiXXXXX:
	ld	bc,-10000
	call Dec16ASC1
print16bitDec2asciiXXXX:
	ld	bc,-1000
	call Dec16ASC1
print16bitDec2asciiXXX:	
	ld	bc,-100
	call Dec16ASC1
print16bitDec2asciiXX:	
	ld	c,-10
	call	Dec16ASC1
	ld	c,-1
Dec16ASC1:
  ld	a,'0'-1
Dum16ASC2:
	inc	a
	add	hl,bc
	jr	c, Dum16ASC2
	sbc	hl,bc
	call PRINTCHR
	ret
	
; Prints 8bit Number in A as decimal ASCII
; Input: A = number
; Output: ASCII (padded 056)
; Destroys BC, HL
print8bitDec2asciiXXX:
	ld	c,-100
	call	Na1
print8bitDec2asciiXX:
	ld	c,-10
	call	Na1
	ld	c,-1
Na1:	
	ld	b,'0'-1
Na2:	
	inc	b
	add	a,c
	jr	c,Na2
	sub	c				;works as add 100/10/1
	push af				;safer than ld c,a
	ld	a,b				;char is in b
	call PRINTCHR		
	pop af				;safer than ld a,c
	ret


; Convert decimal to Ascii HEX (8 bit)
; Input: A = number to convert
; Destroys: BC, A
; Output DE
decToHexAscii:
    ld c, a
    call Num1
    ld d, a
    ld a, c
    call Num2
    ld e, a
    ret
Num1:
    rra
    rra
    rra
    rra
Num2:
    or $F0
    daa
    add a, $A0
    adc a, $40 ; Ascii hex at this point (0 to F)
    ret
    
    
; Prints address as ascii
; Input: HL contains address to be printed
; Destroys: A, HL
; Output: Prints ASCII
print16ToHexAscii:
    ld a,h
    srl a
    srl a
    srl a
    srl a
    cp a,10
    jp m,h3digit
    add a,7
h3digit:
    add a,30h
    call PRINTCHR
    ld a,h
    and a,0fh
    cp a,10
    jp m,h4digit
    add a,7
h4digit:
    add a,30h
    call PRINTCHR
    ld a,l              ; And again with the low half
    srl a
    srl a
    srl a
    srl a
    cp a,10
    jp m,h5digit
    add a,7
h5digit:
    add a,30h
    call PRINTCHR
    ld a,l
    and a,0fh
    cp a,10
    jp m,h6digit
    add a,7
h6digit:
    add a,30h
    call PRINTCHR
    ret
    
 ; Print HEX
 ; Input DE
print8bitHex:
    ld a, d
    call PRINTCHR
    ld a, e
    call PRINTCHR
    ld a,' '
    call PRINTCHR
    ret

; Debug HEX of A
debugHEX:
	push af
	push hl
	push de
	call printDecToHexAscii
	pop de
	pop hl
	pop af
	ret

; debug DEC of A
debugDEC:
	push af
	push hl
	push de
	call print8bitDec2asciiXXX
	pop de
	pop hl
	pop af
	ret

; Print HEX of A
; Destroys DE, HL, A
printDecToHexAscii:
	call decToHexAscii
	ld hl, de
	call print8bitHex
	ret

; Print [
printOpenBracket:
	push af
	ld a, '['
	call PRINTCHR
	pop af
	ret

; Print [
printCloseBracket:
	push af
	ld a, ']'
	call PRINTCHR
	pop af
	ret