

; ------- VT100 --------

BLACK_TXT	.DB ESC,"[30m",EOS
WHITE_TXT	.DB ESC,"[37m",EOS
GREEN_TXT	.DB ESC,"[32m",EOS

BLACK_BAK:	.DB ESC,"[40m",EOS
WHITE_BAK:	.DB ESC,"[47m",EOS
GREEN_BAK:	.DB ESC,"[42m",EOS

BLINK		.DB ESC,"[5m",EOS
TURN_OFF:	.DB ESC,"[0m",EOS
BOLD:		.DB ESC,"[1m",EOS
UNDERLINE	.DB	ESC,"[4m",EOS

HOME:		.DB ESC,"[H",EOS
CLS:		.DB ESC,"[2J",EOS
INVERSE:	.DB ESC,"[7m",EOS

NEXTLINE:	.DB ESC, "[E",EOS

; -------  TEXT ------- 

header: 		.DB " AW Editor 1.0", EOS
newDoc:			.DB "New Document", EOS

PROMPT:     	.DB ":", EOS

OK: 	    	.DB "OK", CR, LF, EOS

RESET:			.DB ESC,"[2J", ESC, "[H", EOS

MENU:			.DB ESC,"[7m^W",ESC,"[0m Save  ",ESC,"[7m^E",ESC,"[0m Exit  "
				.DB ESC,"[7m^K",ESC,"[0m Clear", EOS
				
SAVE:			.DB ESC,"[42m Saved ",ESC,"[0m",EOS

CLEARSAVE:		.DB ESC,"[0m        ",EOS


POSITION:		.DB ESC,"[40;28H ",ESC,"[7mPos",ESC,"[0m 00:00", EOS 
				; Show char position in list item
				
LISTPTR:		.DB ESC,"[40;66H", ESC,"[7mPtr",ESC,"[0m 0000:00", EOS 
				; Show list item address


; ------- Navigation -------

moveDown	.DB	ESC, "[1B", EOS
moveUp		.DB	ESC, "[1A", EOS
moveRight	.DB	ESC, "[1C", EOS
moveLeft	.DB	ESC, "[1D", EOS