; Text Editor Retro Project
; Application address: 0x8200

CR      	.equ    0x0D
LF      	.equ    0x0A
ESC			.equ	0x1B
EOS			.equ	0x00	; End of string
TAB			.equ	0x09
BACK		.equ	0x7F
NULL		.equ	0xFF	; List terminator

; Control chars
CTRLW		.equ	0x17
CTRLE		.equ	0x05
CTRLK		.equ	0x0B

; Navigation (arrows)
NAV			.equ	0x5B
UP			.equ	0x41	; x1b\x5b\x41
DOWN		.equ	0x42	; x1b\x5b\x42
RIGHT		.equ	0x43	; x1b\x5b\x43
LEFT		.equ	0x44	; x1b\x5b\x44

; Editor 
INSERT		.equ	0x7E	; x1b\x5b\x32\x7e

; Coords storage
POSV		.equ	0x9000
POSH		.equ	0x9001

; Flags
IS_SAVED	.equ	0x9002
IS_EXIT		.equ	0x9003

; Document storage
LSTPTR		.equ	0x9004	; Current list pointer address
LSTPOS		.equ	0x9006	; Current list position
DOCBUF		.equ	0x9020	; Start of doc (list)
DOCSIZ		.equ	0x4000	; 8K

; Editor settings
WIDTH		.equ	80
HEIGHT		.equ	23

; BIOS calls
PRINTCHR	.equ 	0x08	; Input: A
PRINTSTR	.equ 	0x10	; Input: HL, Terminate EOS
GETCHR		.equ 	0x18	; Output: A
GETSTR		.equ 	0x20	; Output: STRBUF, Terminate EOS
BIOS		.equ	0x80	; Returns to command prompt
	
; Init
.org 8200h

start:

	call showBanner
	call showMenuCommands
	call showPosStatusText
	call showListPtrText
	
	call initList
	
	call showTermPosStatus
	call showListPtrAddress
	call showListPosStatus
	call goToLastSavedPos

	call loadDocument
	
; wait for instructions
waitForChr:

	; wait for response
	call getKey
	
	; is exit
	ld a, (IS_EXIT)
	cp 1
	jp z, exitEditor

	; back to wait
	jp waitForChr

; Exit to Terminal
exitEditor:

	; clear other flags
	call clearFlags
	
	; clear pointers
	call clearPointers

	; clear terminal
	ld hl, RESET
	call PRINTSTR
	
	jp fastExit

; exit and skip reset
exitDebug:
	call clearFlags
	call clearPointers
	jp fastExit

; Clear Pointers
clearPointers:

	ld hl, LSTPTR
	ld (hl), 0
	inc hl
	ld (hl), 0
	
	ld hl, LSTPOS
	ld (hl), 0
	
	ld hl, POSV
	ld (hl), 0
	
	ld hl, POSH
	ld (hl), 0
	ret

; Clear flags
clearFlags:
	ld a, 0
	ld (IS_SAVED), a
	ld (IS_EXIT), a
	ret

; Exit to Prompt
fastExit:
	jp BIOS
	
; Includes
#include "text.asm"
#include "functions.asm"
#include "utils.asm"
#include "editor.asm"
#include "nav.asm"
#include "list.asm"