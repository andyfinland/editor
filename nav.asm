; Navigations	
handleNavKeys:

    call GETCHR
    
    cp DOWN
	jp z, handleDown
	
	cp UP
	jp z, handleUp
	
	cp RIGHT
	jp z, handleRight
	
	cp LEFT
	jp z, handleLeft
	
    ret

; DOWN
handleDown:
	ret
	
; UP
handleUp:
	ret
	
; RIGHT
handleRight:
	ret
	
; LEFT
handleLeft:
	ret

; reset H pos
resetHCoord:
	push hl
	ld hl, POSH
	ld (hl),1
	pop hl
	ret
	
resetVCoord:
	push hl
	ld hl, POSV
	ld (hl),2
	pop hl
	ret

; inc H
incHCoord:
	push hl
	ld hl, POSH
	inc (hl)
	pop hl
	ret

; dec H
decHCoord:
	push hl
	ld hl, POSH
	dec (hl)
	pop hl
	ret

; inc V
incVCoord:
	push hl
	ld hl, POSV
	inc (hl)
	pop hl
	ret
	
; dec V
decVCoord:
	push hl
	ld hl, POSV
	dec (hl)
	pop hl
	ret
	
; set Horiz pos to end of line
setPosEndLine:
	push hl
	ld hl, POSH
	ld (hl), WIDTH
	pop hl
	ret
	
; Set start pos
setStartPos:
	push af
	ld a, 2
	ld (POSV), a
	ld a, 1
	ld (POSH), a
	pop af
	ret
