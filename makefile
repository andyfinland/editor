
# App starts at 0x8200

FILE=main
ZASM = ~/z80editor2/zasm
APPMAKE = ~/z80editor2/z88dk-appmake
HEXDUMP = ~/z80editor2/hexdump
STARTADDRESS = 0x8200

include ~/Dev/makefile/asm/makefile-asm-z80editor