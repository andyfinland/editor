; FUNCTIONS


; Print X chars B times
; Input A=char, B = count
printXchr:
	call PRINTCHR
	djnz printXchr
	ret
	
; move cursor to starting point
moveCursorHome:
	push de
	ld d, 2
	ld e, 1
	call moveCursor
	pop de
	ret 

; Move cursor to begining of next line
moveCursorBeginNextLine:
	push hl
	push de
	ld hl, POSV
	ld d, (hl)
	inc d
	ld e, 1
	call moveCursor
	pop de
	pop hl
	ret

; Go to last saved position
goToLastSavedPos:
	push hl
	push de
	ld hl, POSV
	ld d, (hl)
	ld hl, POSH
	ld e, (hl)
	call moveCursor
	pop de
	pop hl
	ret

; show footer menu commands
showMenuCommands:
	push de
	push hl
	ld d, 40
	ld e, 1
	call moveCursor
	ld hl, MENU
	call PRINTSTR
	pop hl
	pop de
	ret


; Show terminal position status
showListPtrText:
	push hl
	ld hl,LISTPTR
	call PRINTSTR
	pop hl
	ret
	
; Show pointer address
showListPtrAddress:
	push de
	push hl
	ld d, 40
	ld e, 70
	call moveCursor
	ld hl, (LSTPTR)
	call print16ToHexAscii
	pop hl
	pop de
	ret
	
; Show list position in pointer address
showListPosStatus:
	push hl
	push de
	push af
	ld d, 40
	ld e, 75
	call moveCursor
	ld a, (LSTPOS)
	call print8bitDec2asciiXX
	pop af
	pop de
	pop hl
	ret
	
; Show terminal position status text
showPosStatusText:
	push hl
	ld hl,POSITION
	call PRINTSTR
	pop hl
	ret
	
; Show terminal Position
showTermPosStatus:
	push af
	push de
	push hl
	push bc
	ld d, 40
	ld e, 33
	call moveCursor
	ld a, (POSV)
	call print8bitDec2asciiXX
	ld d, 40
	ld e, 36
	call moveCursor
	ld a, (POSH)
	call print8bitDec2asciiXX
	pop bc
	pop hl
	pop de
	pop af
	ret

; Move to cursor position 
; Input line (v) = d, col (h) = e
moveCursor:
	push hl
	push de
	push af
	
	ld a, ESC
	call PRINTCHR
	ld a, '['
	call PRINTCHR
	
	ld a, d ; row
	call print8bitDec2asciiXX
	
	ld a, ';'
	call PRINTCHR
	
	ld a, e	; col
	call print8bitDec2asciiXX
	
	ld a, 'H'
	call PRINTCHR
	
	pop af
	pop de
	pop hl
	
	ret
	
; show banner for editor
showBanner:

	push hl
	push af

	ld hl, CLS
	call PRINTSTR
	ld hl, HOME
	call PRINTSTR
	ld hl, INVERSE
	call PRINTSTR
	
	ld hl, header
	call PRINTSTR
	
	ld a, ' '
	ld b, 20
	call printXchr
	
	ld hl, newDoc
	call PRINTSTR
	
	ld a, ' '
	ld b, 34
	call printXchr
	
	ld hl, TURN_OFF
	call PRINTSTR
	
	pop af
	pop hl
	
	ret
	
; Show saved message status
showSavedMessage:
	ld hl, SAVE
	push hl
	ld d, 40
	ld e, 50
	call moveCursor
	pop hl
	call PRINTSTR
	ret
	
; clear saved status
clearSaveStatus:

	push af
	push bc
	push de
	push hl
	
	; mark save flag as 0
	ld hl, IS_SAVED
	ld (hl), 0
	
	; update status
	ld d, 40
	ld e, 50
	call moveCursor
	ld hl, CLEARSAVE
	call PRINTSTR
	
	pop hl
	pop de
	pop bc
	pop af
	
	ret

; Print newline
printNewline:
	ld a, CR
	call PRINTCHR
	ld a, LF
	call PRINTCHR
	ret

