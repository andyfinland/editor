; List struct

; Init list
initList:
	call setStartListPtrAddress
	call setStartListPos
	call setStartPos
	ret


	
; Add char to list
; Input A
; Destroys HL, BC
addItemChr:

	push hl
	push bc
	ld hl, (LSTPTR)
	ld bc, (LSTPOS)
	add hl, bc
	dec hl
	ld (hl),a
	pop bc
	pop hl
	ret

; Add address to next list item
; Destroys DE, HL, BC, A
addListItem:
	
	push hl
	push de
	push bc
	
	ld hl, (LSTPTR)
	ld bc, (LSTPOS)
	add hl, bc
	ld de, hl; DE = current pos
	inc de ; DE = new pos
	dec hl
	ld bc, hl
	ld (hl),de
	ld (LSTPTR), de
	
	pop bc
	pop de
	pop hl
	
	ret

; Load document
; <text><EOS><next address>|<null>
loadDocument:
	ld hl, DOCBUF
	ld (LSTPTR), hl
	
	; is new doc then get next char
	ld de, hl
	inc de
	inc de
	ld a, (de)
	cp EOS
	ret z
	
	; else load text
	call showListItems
	ret
	
; Iterate through list items
; Input HL (list item address)
showListItems:
	
	; is end of string
	ld a, (hl)
	cp EOS
	jp z, getNextListItem
	
	ld a, (hl)
	call PRINTCHR
	
	; inc list pos
	call incListPos
	call incHCoord
	
	; goto next char
	inc hl
	jp showListItems
	
getNextListItem:
	
	; get next byte
	inc hl

	; is it NULL (end of list struc)
	ld a, (hl)
	cp NULL
	jp z, exitListItem	
	
	; Otherwise get next list item address
	ld a, (hl)
	ld e, a
	inc hl
	ld a, (hl)
	ld d, a
	ld (LSTPTR), de
	ld hl, de
	
	; update list pos
	call setStartListPos
	
	; inc v pos status
	call incVCoord
	call resetHCoord
	
	; goto next list item
	jp showListItems

; Finish loading doc
; <text><EOS><next address>|<null>
exitListItem:

	; replace null with next list item address
	; HL = start of next pointer
	
	ld de, hl
	
	inc de
	inc de
	
	ld (hl), de
	
	inc hl
	inc hl
	
	ld (LSTPTR), hl
	
	; update list pos
	call setStartListPos
	
	; inc v pos status
	call incVCoord
	call resetHCoord
	
	; show status
	call showTermPosStatus
	call showListPtrAddress
	call showListPosStatus
	call goToLastSavedPos
	
	ret

; Remove list item
removeListItem:
	ret

; Inc position in list item
incListPos:
	push hl
	ld hl, LSTPOS
	inc (hl)
	pop hl
	ret
	
; Dec position in list item
decListPos:
	push hl
	ld hl, LSTPOS
	dec (hl)
	pop hl
	ret

; set list position to begining
setStartListPos:
	push hl
	ld hl, LSTPOS
	ld (hl),1
	pop hl
	ret

; set list pointer to start of doc buffer
setStartListPtrAddress:
	push hl
	ld hl, DOCBUF
	ld (LSTPTR), hl
	pop hl
	ret